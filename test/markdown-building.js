import * as markdownBuilding from '../lib/building/markdown'
import creole from "../lib/creoleparser"
import should from "should"
import fs from "fs"


describe("Test CreoleParser with single elements", function() {

  describe("#parse()", function() {
    // it("execute without errors", function(done) {
    //   let ret = creole.parse("")
    //   done()
    // })

    // it("return text only", function(done) {
    //   let wiki = "Hello world"
    //   let parsed = creole.parse(wiki, { inline: true })
    //   parsed.formatted.should.equal(wiki)
    //   parsed.text.should.equal(wiki)
    //   done()
    // })

    // it("return html p", function(done) {
    //   let wiki = "Hello world"
    //   let html = `<p>${wiki}</p>`
    //   let parsed = creole.parse(wiki)
    //   parsed.formatted.should.equal(html)
    //   parsed.text.should.equal(wiki)
    //   done()
    // })

    // it("return html em", function(done) {
    //   let wiki = "//Hello world//"
    //   let html = "<em>Hello world</em>"
    //   let text = "Hello world"
    //   let parsed = creole.parse(wiki, { inline: true })
    //   parsed.formatted.should.equal(html)
    //   parsed.text.should.equal(text)
    //   done()
    // })

    // it("return html em inside p", function(done) {
    //   let wiki = "//Hello world//"
    //   let html = "<p><em>Hello world</em></p>"
    //   let text = "Hello world"
    //   let parsed = creole.parse(wiki)
    //   parsed.formatted.should.equal(html)
    //   parsed.text.should.equal(text)
    //   done()
    // })

    it("return an html link", function(done) {
      let wiki = "[[http://www.google.fr/]]"
      let html = "<a href=\"http://www.google.fr/\"></a>"
      let parsed = creole.parse(wiki, { building: markdownBuilding, inline: true })
      console.log(parsed)
      // parsed.formatted.should.equal(html)
      done()
    })

    it("return an html link inside p", function(done) {
      let wiki = "[[http://www.google.fr/|Google]]"
      let html = "<a href=\"http://www.google.fr/\">Google</a>"
      let parsed = creole.parse(wiki, { building: markdownBuilding, inline: true })
      console.log(parsed)

      // parsed.formatted.should.equal(html)
      done()
    })

    it("return an html link with rel", function(done) {
      let wiki = "[[http://www.google.fr/|Google]~test]"
      let html = "<a rel=\"test\" href=\"http://www.google.fr/\">Google</a>"
      let parsed = creole.parse(wiki, { building: markdownBuilding, inline: true })
      console.log(parsed)

      // parsed.formatted.should.equal(html)
      done()
    })

//     it("return an html dl", function(done) {
//       let wiki = `; First title of definition list
// : Definition of first item.
// ; Second title: Second definition
// beginning on the same line.`
//       let html = "<dl>\n  <dt>First title of definition list</dt>\n    <dd>Definition of first item.</dd>\n  <dt>Second title</dt>\n    <dd>Second definition\nbeginning on the same line.</dd>\n</dl>\n"
//       let text = "First title of definition listDefinition of first item.Second titleSecond definitionbeginning on the same line."
//       let parsed = creole.parse(wiki)
//       parsed.formatted.should.equal(html)
//       parsed.text.should.equal(text)
//       done()
//     })

  })

})

// describe("Test CreoleParser with realistic content", function() {

//   describe("#parse()", function() {
//     // it "short text 0 behavior", (done) ->
//     //   wiki = fs.readFileSync("./test/data/test_text.wiki").toString()
//     //   html = fs.readFileSync("./test/data/test_text.html").toString()
//     //   text = fs.readFileSync("./test/data/test_text.text").toString()
//     //   parsed = creole.parse(wiki, { autoBr: true })
//     // #   html = fs.writeFileSync("./test/data/test_text.html",parsed.formatted)
//     // #   text = fs.writeFileSync("./test/data/test_text.text",parsed.text)
//     //   parsed.formatted.should.equal html
//     //   parsed.text.should.equal text
//     //   done()
//     //   return

//     it("short text 1 behavior", function(done) {
//       let wiki = fs.readFileSync("./test/data/test_text1.wiki").toString()
//       let html = fs.readFileSync("./test/data/test_text1.html").toString()
//       let text = fs.readFileSync("./test/data/test_text1.text").toString()
//       let parsed = creole.parse(wiki, { inline: true, autoBr: true })
//       // html = fs.writeFileSync("./test/data/test_text1.html",parsed.formatted)
//       // text = fs.writeFileSync("./test/data/test_text1.text",parsed.text)
//       // console.log "html:", parsed.formatted
//       // console.log "text:", parsed.text
//       parsed.formatted.should.equal(html)
//       parsed.text.should.equal(text)
//       done()
//     })

//     it("short text 2 behavior", function(done) {
//       let wiki = fs.readFileSync("./test/data/test_text2.wiki").toString()
//       let html = fs.readFileSync("./test/data/test_text2.html").toString()
//       let text = fs.readFileSync("./test/data/test_text2.text").toString()
//       let parsed = creole.parse(wiki, { inline: true, autoBr: true })
//       // html = fs.writeFileSync("./test/data/test_text2.html",parsed.formatted)
//       // text = fs.writeFileSync("./test/data/test_text2.text",parsed.text)
//       // console.log "html:", parsed.formatted
//       // console.log "text:", parsed.text
//       parsed.formatted.should.equal(html)
//       parsed.text.should.equal(text)
//       done()
//     })
//     //
//     it("short text 3 behavior", function(done) {
//       let wiki = fs.readFileSync("./test/data/test_text3.wiki").toString()
//       let html = fs.readFileSync("./test/data/test_text3.html").toString()
//       let text = fs.readFileSync("./test/data/test_text3.text").toString()
//       let parsed = creole.parse(wiki, { inline: true, autoBr: true })
//       // html = fs.writeFileSync("./test/data/test_text3.html",parsed.formatted)
//       // text = fs.writeFileSync("./test/data/test_text3.text",parsed.text)
//       // console.log "html:", parsed.formatted
//       // console.log "text:", parsed.text
//       parsed.formatted.should.equal(html)
//       parsed.text.should.equal(text)
//       done()
//     })
//     //
//     it("short text 4 behavior", function(done) {
//       let wiki = fs.readFileSync("./test/data/test_text4.wiki").toString()
//       let html = fs.readFileSync("./test/data/test_text4.html").toString()
//       let text = fs.readFileSync("./test/data/test_text4.text").toString()
//       let parsed = creole.parse(wiki, { autoBr: true })
//       // html = fs.writeFileSync("./test/data/test_text4.html",parsed.formatted)
//       // text = fs.writeFileSync("./test/data/test_text4.text",parsed.text)
//       parsed.formatted.should.equal(html)
//       parsed.text.should.equal(text)
//       done()
//     })

//   })

// })
