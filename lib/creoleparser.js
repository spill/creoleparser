import _ from "lodash"
import XRegExp from 'xregexp'
import * as htmlBuilding from './building/html'

const debug = require("debug")("creoleparser")

let CreoleParser = module.exports = {}

let chunks = undefined
let initialized = 0

let parse = function(content, chunk, autoBr) {
  debug("============================================")
  debug(` #parse with chunk: ${chunk} autoBr: ${autoBr} content.length: ${content.length} with content: ${content}`)
  let formattedResult = ""
  let textResult = ""
  let ch = undefined
  let pos = 0
  let lpos = 0

  while (true) {
    if (ch) { // if we already know what kind of chunk this is

      debug(` * treat elem ${ch}`)
      debug(` - will try to match delim (${chunks[ch]["delim"].source}) from lpos: ${lpos}`)

      let matched = XRegExp.exec(content.substring(lpos),chunks[ch]["delim"]); // find where it stops...

      if (!matched) {
        pos = content.length
        debug(` - no end position matched so use the end: ${pos}`)
      } else {
        if (chunks[ch]["delim"].lastIndex > 0) {
          pos = lpos + chunks[ch]["delim"].lastIndex; //     another chunk // => XXX: use of textResult ?!.
          debug(` - end position matched: ${pos} with: `, matched)
        } else if (lpos <= content.length) {
          lpos = lpos + 1
          debug(` - continue to lpos: ${lpos}`)
          continue
        } else {
          debug(" - break on parse")
          break
        }
      }

      let open = undefined
      let close = undefined

      let t = content.substring(lpos, pos)

      debug(` - substring matched (from lspos: ${lpos} to pos: ${pos}): ${t}`)

      if (chunks[ch]["filter"]) { // filter it, if applicable
        var ret = chunks[ch]["filter"](t, { parent: chunk, autoBr: autoBr })
        if (_.isObject(ret)) {
          t = ret.content
          if (_.has(ret, 'open')) {
            open = ret.open
          }
          if (_.has(ret, 'close')) {
            close = ret.close
          }
        } else {
          t = ret
        }
      }

      debug(` - filtered string: ${t}`)

      if (chunks[ch]["open"] === "\n" && autoBr) {
        formattedResult += "<br />"; // print the open tag
      } else if (open) {
        formattedResult += open; // print the open tag
      } else {
        formattedResult += chunks[ch]["open"]; // print the open tag
      }
      lpos = pos; // remember where this chunk ends (where next begins)

      if (t && chunks[ch]["contains"]) { // if it contains other chunks...
        debug(" - contains detected so recurse")
        var ret = parse(t, ch, autoBr)
        formattedResult += ret.formatted; // subhtml
        textResult += ret.text; // subtext
      } else {
        formattedResult += t; //    otherwise, print it
        textResult += t; //    otherwise, print it
      }
      if (close) {
        formattedResult += close; // print the close
      } else {
        formattedResult += chunks[ch]["close"]; // print the close
      }

      debug("==================")
      debug(` - has now in html: ${formattedResult}`)
      debug(` - has now in text: ${formattedResult}`)
      debug("==================")
    }

    if (pos && pos === content.length) {
      debug(` # Everything is eaten ! (content.length: ${content.length}): ${content.substring(pos)}`)

      // we've eaten the whole string
      break
    } else {
      ch = undefined

      // more string to come
      debug(` * search for known pattern from pos: ${pos}`)

      _.each(chunks[chunk]["contains"], function(elem) { // check all possible chunks
        if (XRegExp.exec(content.substring(pos), chunks[elem]["curpatcmp"])) {
          debug(` - match ${elem}`)
          ch = elem
          return false
        }
      })

      if (!ch) { return false; } // no idea what this is.  ditch the rest && give up.
    }
  }
  return {
    formatted: formattedResult,
    text: textResult
  }
}

// compile a regex that matches any of the patterns that interrupt the
// current chunk.
let delim = function(elem) {
  if (Array.isArray(chunks[elem]["stops"])) {
    let regexElements = []
    _.forEach(chunks[elem]["stops"], function(stopElem) {
      if (chunks[stopElem]["fwpat"]) {
        regexElements.push(chunks[stopElem]["fwpat"])
      } else {
        regexElements.push(chunks[stopElem]["curpat"])
      }
    })
    let fullPattern = regexElements.join("|")
    debug(` -> set delim for elem: ${elem} =${fullPattern}`)
    return XRegExp(fullPattern, "g")
  } else {
    debug(` -> set delim for elem: ${elem} =${chunks[elem]["stops"]}`)
    return XRegExp(chunks[elem]["stops"], "g")
  }
}

// one-time optimization of the grammar - speeds the parser up a ton
initialized = undefined
let init = function(options) {
  // XXX: initialized should not be global need to fix scope !
  if (initialized && initialized.building === options.building.name) {
    return
  }

  initialized = {
    building: options.building.name
  }

  chunks = options.building.chunks

  // precompile a bunch of regexes
  for (let c in chunks) {
    if (chunks[c]["curpat"]) { chunks[c]["curpatcmp"] = XRegExp(`^${chunks[c]["curpat"]}`, 's'); }
    if (chunks[c]["stops"]) { chunks[c]["delim"] = delim(c); }
  }

}

const defaults = {
  inline: false,
  autoBr: false,
  building: htmlBuilding
}

CreoleParser.parse = function(content, opts) {
  const options = _.assign({}, defaults, opts)
  if (!content || content.length <= 0 || content === "") { return; }
  init(options)

  return parse(content, options.inline ? 'line' : 'top', options.autoBr)
}


