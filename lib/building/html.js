import _ from "lodash"
import { 
  stripHeaderTrigger,
  stripListTrigger,
  emptyStr,
  specialChars,
  plainChars,
  eol,
  bol
} from '../common'

const inlineElts = [
  "strong",
  "em",
  "br",
  "autobr",
  "esc",
  "img",
  "linkrel",
  "link",
  "ilink",
  "inowiki",
  "sub",
  "sup",
  "mono",
  "u",
  "tm",
  "span",
  "reg",
  "copy",
  "ndash",
  "ellipsis",
  "amp"
]

const allInlineElts = _.flatten([
  inlineElts,
  "plain",
  "any"
])

const blocks = [
  "h1",
  "h2",
  "h3",
  "hr",
  "nowiki",
  "h4",
  "h5",
  "h6",
  "ul",
  "ol",
  "dl",
  "p",
  "blank"
]

let linkHandler = undefined
let barelinkHandler = undefined
let imgHandler = undefined

export const name = 'html'
export const chunks = {
  line: {
    contains: allInlineElts
  },

  top: {
    contains: blocks
  },

  blank: {
    curpat: `(?= *${eol})`,
    fwpat: `(?=(?:^|\\n) *${eol})`,
    stops: "(?=\\S)",
    filter: emptyStr,
    open: "",
    close: ""
  },

  p: {
    curpat: "(?=.)",
    stops: [
      "blank",
      "h",
      "hr",
      "nowiki",
      "ul",
      "ol",
      "dl"
    ],
    contains: allInlineElts,
    filter: content => content.trim(),
    open: "<p>",
    close: "</p>"
  },

  ip: {
    curpat: "(?=:)",
    fwpat: "\\n(?=:)",
    stops: [
      "blank",
      "h",
      "hr",
      "nowiki",
      "ul",
      "ol",
      "dl",
      "table"
    ],
    contains: [
      "p",
      "ip"
    ],
    filter: (content =>
      content
        .replace(/:/, "")
        .replace(/\n:/, "\n")
    ),
    open: "<div style=\"margin-left: 2em\">",
    close: "</div>\n"
  },

  dl: {
    curpat: "\\n*(?=;)",
    fwpat: "\\n(?=;)",
    stops: [
      "blank",
      "h",
      "hr",
      "nowiki",
      "ul",
      "ol"
    ],
    contains: [
      "dt",
      "dd"
    ],
    open: "<dl>\n",
    close: "</dl>\n"
  },

  dt: {
    curpat: "(?=;)",
    fwpat: "\\n(?=;)",
    stops: "(?=:|\\n[^:])",
    contains: allInlineElts,
    filter: (content =>
      content
        .replace(/^;?\s*/, "")
        .replace(/(\s*|\n*)$/, "")
    ),
    open: "  <dt>",
    close: "</dt>\n"
  },

  dd: {
    curpat: "(?=:)",
    fwpat: "(?:\\n|:)",
    stops: "(?=:)|\\n(?=;)",
    contains: allInlineElts,
    filter: content =>(
      content
        .replace(/^(?:\n|:)?\s*/, "")
        .replace(/\s*$/, "")
    ),
    open: "    <dd>",
    close: "</dd>\n"
  },

  table: {
    curpat: "(?= *\\|.)",
    fwpat: "\\n(?= *\\|.)",
    stops: "\\n(?= *[^\\|])",
    contains: ["tr"],
    open: "<table>\n",
    close: "</table>\n\n"
  },

  tr: {
    curpat: "(?= *\\|)",
    stops: "\\n",
    contains: [
      "td",
      "th"
    ],
    filter: (content =>
      content
        .replace(/^ */, "")
        .replace(/\| *$/, "")
    ),
    open: "    <tr>\n",
    close: "    </tr>\n"
  },

  td: {
    curpat: "(?=\\|[^=])",
    stops: "[^~](?=\\|(?!(?:[^\\[]*\\]\\])|(?:[^\\{]*\\}\\})))",
    contains: allInlineElts,
    filter: (content =>
      content
        .replace(/^ */, "")
        .replace(/\s*$/, "")
    ),
    open: "        <td>",
    close: "</td>\n"
  },

  th: {
    curpat: "(?=\\|=)",
    stops: "[^~](?=\\|(?!(?:[^\\[]*\\]\\])|(?:[^{]*\\}\\})))",
    contains: allInlineElts,
    filter: (content => 
      content
        .replace(/^ */, "")
        .replace(/\s*$/, "")
    ),
    open: "        <th>",
    close: "</th>\n"
  },

  ul: {
    curpat: "(?=(?:`| *)\\*[^\\*])",
    fwpat: "(?=\n(?:`| *)\\*[^\\*])",
    stops: [
      "blank",
      "h",
      "nowiki",
      "li",
      "hr",
      "dl"
    ],
    contains: [
      "ul",
      "ol",
      "li"
    ],
    filter: stripListTrigger,
    open: "<ul>\n",
    close: "</ul>\n"
  },

  ol: {
    curpat: "(?=(?:`| *)\\#[^\\#])",
    fwpat: "(?=\\n(?:`| *)\\#[^\\#])",
    stops: [
      "blank",
      "h",
      "nowiki",
      "li",
      "hr",
      "dl"
    ],
    contains: [
      "ul",
      "ol",
      "li"
    ],
    filter: stripListTrigger,
    open: "<ol>\n",
    close: "</ol>\n"
  },

  li: {
    curpat: "(?=`[^\\*\\#])",
    fwpat: "\\n(?=`[^\\*\\#])",
    stops: "\\n(?=`)",
    filter: content => content.replace(/` */, "").trim(),
    contains: allInlineElts,
    open: "    <li>",
    close: "</li>\n"
  },

  nowiki: {
    curpat: "(?=\\{\\{\\{ *\\n)",
    fwpat: "\\n(?=\\{\\{\\{ *\\n)",
    stops: `\\n\\}\\}\\} *${eol}`,
    filter: (content =>
      content
        .substring(3)
        .replace(/\}\}\}\s*$/, "")
        .replace(/&/, "&amp;", "g")
        .replace(/</, "&lt;", "g")
        .replace(/>/, "&gt;", "g")
    ),
    open: "<pre>",
    close: "</pre>\n\n"
  },

  hr: {
    curpat: `(?= *-{4,} *${eol})`,
    fwpat: `\n(?= *-{4,} *${eol})`,
    stops: eol,
    open: "<hr />\n\n",
    close: "",
    filter: emptyStr
  },

  h: {
    curpat: "(?=(?:^|\\n) *=)"
  },

  h1: {
    curpat: "(?= *=[^=])",
    stops: "\\n",
    contains: allInlineElts,
    open: "<h1>",
    close: "</h1>\n\n",
    filter: stripHeaderTrigger
  },

  h2: {
    curpat: "(?= *={2}[^=])",
    stops: "\\n",
    contains: allInlineElts,
    open: "<h2>",
    close: "</h2>\n\n",
    filter: stripHeaderTrigger
  },

  h3: {
    curpat: "(?= *={3}[^=])",
    stops: "\\n",
    contains: allInlineElts,
    open: "<h3>",
    close: "</h3>\n\n",
    filter: stripHeaderTrigger
  },

  h4: {
    curpat: "(?= *={4}[^=])",
    stops: "\\n",
    contains: allInlineElts,
    open: "<h4>",
    close: "</h4>\n\n",
    filter: stripHeaderTrigger
  },

  h5: {
    curpat: "(?= *={5}[^=])",
    stops: "\\n",
    contains: allInlineElts,
    open: "<h5>",
    close: "</h5>\n\n",
    filter: stripHeaderTrigger
  },

  h6: {
    curpat: "(?= *={6,})",
    stops: "\\n",
    contains: allInlineElts,
    open: "<h6>",
    close: "</h6>\n\n",
    filter: stripHeaderTrigger
  },

  plain: {
    curpat: "(?=[^\\*/_,\\^\\\\{\\[<\\|])",
    stops: [
      "strong",
      "em",
      "br",
      "autobr",
      "esc",
      "img",
      "linkrel",
      "link",
      "ilink",
      "inowiki",
      "sub",
      "sup",
      "mono",
      "u",
      "tm",
      "span",
      "reg",
      "copy",
      "ndash",
      "ellipsis",
      "amp"
    ],
    open: "",
    close: ""
  },

  any: {
    curpat: "(?=.)",
    stops: inlineElts,
    open: "",
    close: ""
  },

  br: {
    curpat: "(?=\\\\\\\\)",
    stops: "\\\\\\\\",
    filter: emptyStr,
    open: "<br />",
    close: ""
  },

  autobr: {
    curpat: "(?=\\n)",
    stops: "\\n",
    filter: emptyStr,
    open: "\n",
    close: ""
  },

  esc: {
    curpat: "(?=~[\\S])",
    stops: "~.",
    filter: content => content.substring(1),
    open: "",
    close: ""
  },

  inowiki: {
    curpat: "(?=\\{{3}.*?\\}*\\}{3})",
    stops: ".*?\\}*\\}{3}",
    filter: (content =>
      content
        .substring(3)
        .replace(/\}{3}$/, "")
        .replace(/&/, "&amp;", "g")
        .replace(/</, "&lt;", "g")
        .replace(/>/, "&gt;", "g")
    ),
    open: "<tt>",
    close: "</tt>"
  },

  ilink: {
    curpat: "(?=(?:https?|ftp):\\/\\/)",
    stops: "(?=[!\"\#$%&'()*+,\-./:;<=>?@\[\\\]^_`{|}~]?(?:\\s|$))",
    filter: content => {
      content = content.replace(/^\s*/, "").replace(/\s*$/, "")
      if (barelinkHandler) { content = barelinkHandler(content) }
      return `href="${content}">${content}`
    },
    open: "<a ",
    close: "</a>"
  },

  link: {
    curpat: "(?=\\[\\[[^\\n]+?\\]\\])",
    stops: "]]",
    contains: [
      "href",
      "atext"
    ],
    filter: content => {
      content = content.substring(2).slice(0, -2)
      if (!content.indexOf("|")) {
        let matching = content.match(/^\w+:\/\/(.*)$/)
        content = content + "|" + matching[1]
      }
      return content
    },
    open: "<a ",
    close: "</a>"
  },

  linkrel: {
    curpat: "(?=\\[\\[[^\\n]+?\\]~\\w*\\])",
    stops: "\\]~\\w*\\]",
    contains: [
      "href",
      "atext"
    ],
    filter: content => {
      content = content.substring(2)
      let regex = /\]\~(\w*)\]/
      let matching = content.match(regex)
      let open = ((content = content.replace(regex, "")) && Array.isArray(matching) && matching[1] ? `<a rel="${matching[1]}" ` : false)
      if (!content.indexOf("|")) {
        matching = content.match(/^\w+:\/\/(.*)$/)
        content = content + "|" + matching[1]
      }
      return {
        content: content,
        open: open
      }
    },
    open: "<a rel=\"external\" ",
    close: "</a>"
  },

  href: {
    curpat: "(?=[^\\|])",
    stops: "(?=\\|)",
    filter: content => {
      content = content.replace(/^\s*/, "").replace(/\s*$/, "")
      if (linkHandler) { content = linkHandler(content); }
      return content
    },
    open: "href=\"",
    close: "\">"
  },

  span: {
    curpat: "(?=\\(\\([^\\n]+?\\)(~\\w+)?\\))",
    stops: "\\)(~\\w+)?\\)",
    contains: allInlineElts,
    filter: content => {
      content = content.substring(2)
      let regex = /\)(\~(\w*))?\)/
      let matching = content.match(regex)
      let open = ((content = content.replace(regex, "")) && Array.isArray(matching) && matching[2] ? `<span class="${matching[2]}">` : false)
      return {
        content: content,
        open: open
      }
    },
    open: "<span>",
    close: "</span>"
  },

  atext: {
    curpat: "(?=\\|)",
    stops: "\\n",
    contains: allInlineElts,
    filter: (content =>
      content
        .replace(/^\|\s*/, "")
        .replace(/\s*$/, "")
    ),
    open: "",
    close: ""
  },

  img: {
    curpat: "(?=\\{\\{[^\\{][^\\n]*?\\}\\})",
    stops: "\\}\\}",
    contains: [
      "imgsrc",
      "imgalt"
    ],
    filter: (content =>
      content
        .substring(2)
        .replace(/\}\}$/, "")
    ),
    open: "<img ",
    close: " />"
  },

  imgalt: {
    curpat: "(?=\\|)",
    stops: "\\n",
    filter: (content =>
      content
        .replace(/^\|\s*/, "")
        .replace(/\s*$/, "")
    ),
    open: " alt=\"",
    close: "\""
  },

  imgsrc: {
    curpat: "(?=[^\\|])",
    stops: "(?=\\|)",
    filter: content => {
      content = content.replace(/^\s*/, "").replace(/\s*$/, "")
      if (imgHandler) { content = imgHandler(content); }
      return content
    },
    open: "src=\"",
    close: "\""
  },

  strong: {
    curpat: "(?=\\*\\*)",
    stops: "\\*\\*.*?\\*\\*",
    contains: allInlineElts,
    filter: content => content.substring(2).replace(/\*\*$/, ""),
    open: "<strong>",
    close: "</strong>"
  },

  em: {
    curpat: "(?=\\/\\/)",
    stops: "\\/\\/.*?\\/\\/",
    contains: allInlineElts,
    filter: content => content.substring(2).replace(/\/\/$/, ""),
    open: "<em>",
    close: "</em>"
  },

  mono: {
    curpat: "(?=\\#\\#)",
    stops: "\\#\\#.*?\\#\\#",
    contains: allInlineElts,
    filter: content => content.substring(2).replace(/\#\#$/, ""),
    open: "<tt>",
    close: "</tt>"
  },

  sub: {
    curpat: "(?=,,)",
    stops: ",,.*?,,",
    contains: allInlineElts,
    filter: content => content.substring(2).replace(/\,\,$/, ""),
    open: "<sub>",
    close: "</sub>"
  },

  sup: {
    curpat: "(?=\\^\\^)",
    stops: "\\^\\^.*?\\^\\^",
    contains: allInlineElts,
    filter: content => content.substring(2).replace(/\^\^$/, ""),
    open: "<sup>",
    close: "</sup>"
  },

  u: {
    curpat: "(?=__)",
    stops: "__.*?__",
    contains: allInlineElts,
    filter: content => content.substring(2).replace(/__$/, ""),
    open: "<u>",
    close: "</u>"
  },

  amp: {
    curpat: "(?=\\&(?!\\w+\\;))",
    stops: ".",
    filter: content => "&amp;",
    open: "",
    close: ""
  },

  tm: {
    curpat: "(?=\\(TM\\))",
    stops: "\\(TM\\)",
    filter: content => "&trade;",
    open: "",
    close: ""
  },

  reg: {
    curpat: "(?=\\(R\\))",
    stops: "\\(R\\)",
    filter: content => "&reg;",
    open: "",
    close: ""
  },

  copy: {
    curpat: "(?=\\(C\\))",
    stops: "\\(C\\)",
    filter: content => "&copy;",
    open: "",
    close: ""
  },

  ndash: {
    curpat: "(?=--)",
    stops: "--",
    filter: content => "&ndash;",
    open: "",
    close: ""
  },

  ellipsis: {
    curpat: "(?=\\.\\.\\.)",
    stops: "\\.\\.\\.",
    filter: content => "&hellip;",
    open: "",
    close: ""
  }
}

export function setLinkHandler (handler) {
  if (!handler) { return; }
  linkHandler = handler
}

export function customLinks () {
  chunks["href"]["open"] = ""
  chunks["href"]["close"] = ""
  chunks["link"]["open"] = ""
  chunks["link"]["close"] = ""
  delete (chunks["link"]["contains"])

  chunks["link"]["filter"] = function(content) {
    if (linkHandler) { content = linkHandler(content); }
    return content
  }

}

export function setBarelinkHandler (handler) {
  if (!handler) { return; }
  barelinkHandler = handler
}

export function customBarelinks () {
  chunks["ilink"]["open"] = ""
  chunks["ilink"]["close"] = ""
  chunks["ilink"]["filter"] = function(content) {
    if (barelinkHandler) { content = barelinkHandler(content); }
    return content
  }

}

export function customimgs () {
  chunks["img"]["open"] = ""
  chunks["img"]["close"] = ""
  delete (chunks["img"]["contains"])

  chunks["img"]["filter"] = function(content) {
    if (imgHandler) { content = imgHandler(content); }
    return content
  }

}

export function setImgHandler (handler) {
  if (!handler) { return; }
  imgHandler = handler
}

export function tag (tag, type, text) {
  if (!tag) {
    forEach(sort(_.keys(chunks)), function(elt) {
      let o = chunks[elt]["open"]
      let c = chunks[elt]["close"]
      // XXX: so flag !
      if (!o || !o.match(/</)) { return; } // XXX: return is the right statement to replace a continue ?
      if (o) {
        o = o.replace(/\n/, "\n", "g")
      } else {
        o = ""
      }
      if (c) {
        c = c.replace(/\n/, "\n", "g")
      } else {
        c = ""
      }
    })

  } else {
    if (type !== "open" && type === "close") { return; }
    if (!chunks[tag]) { return; }
    chunks[tag][type] = (text ? text : "")
  }
}
