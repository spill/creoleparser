import _ from "lodash"

// strip lead/trail white/= from headings
export function stripHeaderTrigger(content) {
  return content.replace(/^\s*=*\s*/, "").replace(/\s*=*\s*$/, "")
}

// strip list markup trickery
export function stripListTrigger(content) {
  return content.replace(/(?:`*| *)[\\*\#]/, "`").replace(/\n(?:`*| *)[\\*\#]/, "\n`", "g")
}

export function emptyStr() {
  return ""
}

export const specialChars = [
  "^",
  "\\",
  "*",
  "/",
  "_",
  ",",
  "{",
  "[",
  "<",
  "~",
  "|",
  "\n",
  "#",
  ":",
  ";",
  "(",
  "-",
  "."
]

export const plainChars = []

export const eol = "(?:\\n|$)"
export const bol = "(?:^|\\n)"
